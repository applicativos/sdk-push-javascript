angular.module('starter')
        .factory('WilliartsPushModuloFactory', ['Config', 'RequestModuloFactory', 'ExtraModuloFactory',
            function (Config, RequestModuloFactory, ExtraModuloFactory) {
                "use strict";
                var services = {};
                var xhr = RequestModuloFactory;

                services.config = function () {
                    xhr.header();
                    xhr.url = Config.url_serve_push;
                    xhr.setHeader('X-ApiToken', '$2y$10$RvNOmKTeXtXprcV9U2xLcefD3mfoH406cl4DnZAgTdYe6NB4rWn.m');

                };

                services.uniqSerial = function (serial) {
                    var d = ionic.Platform.device();
                    return ExtraModuloFactory.encode(d.platform + d.model + d.version + d.manufacturer + serial);
                };

                services.info = function (callback) {
                    this.config();
                    xhr.get('apps/view.json', {}, callback);
                };

                services.devices = function (params, callback) {
                    this.config();
                    xhr.get('devices/index.json', params, callback);
                };

                services.devicesAdd = function (params, callback) {
                    this.config();
                    var d = ionic.Platform.device();
                    params = angular.merge({
                        plataforma: d.platform,
                        marca: d.manufacturer,
                        modelo: d.model,
                        cordova: d.cordova,
                        uuid: d.uuid,
                        version: d.version,
                        version_app: Config.appVersion,
                        serial: null,
                        serial_unique: null,
                        status: 1,
                        id_user: null,
                        tipo: (Config.radio_id > 0 ? 'Radio' : 'Cliente')
                    }, params);
                    xhr.post('devices/add.json', params, callback);
                };

                services.devicesEdit = function (id, params, callback) {
                    this.config();
                    xhr.put('devices/edit/' + id + '.json', params, callback);
                };

                services.devicesDelete = function (id, callback) {
                    this.config();
                    xhr.delete('devices/delete/' + id + '.json', {}, callback);
                };

                services.devicesView = function (id, callback) {
                    this.config();
                    xhr.get('devices/view/' + id + '.json', {}, callback);
                };

                services.mensagens = function (params, callback) {
                    this.config();
                    xhr.get('push-mensagens/index.json', params), callback;
                };

                services.mensagensAdd = function (params, callback) {
                    this.config();
                    xhr.post('push-mensagens/add.json', params, callback);
                };

                services.mensagensEdit = function (id, params, callback) {
                    this.config();
                    xhr.put('push-mensagens/edit/' + id + '.json', params, callback);
                };

                services.mensagensDelete = function (id, callback) {
                    this.config();
                    xhr.delete('push-mensagens/delete/' + id + '.json', null, callback);
                };

                services.mensagensView = function (id, callback) {
                    this.config();
                    xhr.get('push-mensagens/view/' + id + '.json', null, callback);
                };

                services.mensagensDevices = function (params, callback) {
                    this.config();
                    xhr.get('push-mensagens-devices/index/.json', params, callback);
                };

                services.mensagensDevicesView = function (id, callback) {
                    this.config();
                    xhr.get('push-mensagens-devices/view/' + id + '.json', null, callback);
                };

                return services;
            }
        ]);